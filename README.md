# EEE3088F PiHat Project
My PiHat functions as an uniterupted power supply (UPS). It keeps whatever it is connected to supplied with power even if main power switches off.

Subsystems Required:

1. Satus LEDs
2. "Voltmeter" or shunt and signal opamp constraining values to 0-3.3V
3. Power circuitry to convert battery voltage to correct Pi Voltages

How to contribute:

1. Upload possible designs for each subsystem with how they work as well as simulations of them working
2. Suggest possible improvements that could be made
3. Identify possible errors that could have been made
